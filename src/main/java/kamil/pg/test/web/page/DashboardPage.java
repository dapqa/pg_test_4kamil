package kamil.pg.test.web.page;

import kamil.pg.test.core.dao.MessageDAO;
import kamil.pg.test.core.dto.Message;
import kamil.pg.test.web.behavior.ModelUpdateBehavior;
import kamil.pg.test.web.page.base.BasePage;
import kamil.pg.test.web.panel.MessagePanel;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.form.AjaxSubmitLink;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.form.*;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.AbstractReadOnlyModel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.spring.injection.annot.SpringBean;

import java.util.List;

/**
 * @author Nechaev Alexander
 */
public class DashboardPage extends BasePage {

    @SpringBean(name = "messageDAO")
    private MessageDAO messageDAO;

    private Form<Message> form;
    private FeedbackPanel feedbackPanel;
    private WebMarkupContainer messagesPaneContainer;

    @Override
    protected void onInitialize() {
        super.onInitialize();

        form = new Form<>("newMessageForm", new CompoundPropertyModel<>(new Message()));
        form.setOutputMarkupId(true);
        add(form);

        form.add(feedbackPanel = new FeedbackPanel("feedback"));
        feedbackPanel.setOutputMarkupId(true);


        form.add(new TextField<String>("authorName")
                .setLabel(Model.of("Подпись"))
                .setRequired(true)
                .add(new ModelUpdateBehavior())
        );

        form.add(new DropDownChoice<>("messageColor", messageDAO.getMessageColors())
                .setNullValid(true)
                .setChoiceRenderer(new ChoiceRenderer<>("name", "id"))
                .setLabel(Model.of("Цвет"))
                .add(new ModelUpdateBehavior())
        );

        form.add(new TextArea<>("content")
                .setRequired(true)
                .setLabel(Model.of("Сообщение"))
                .add(new ModelUpdateBehavior())
        );

        form.add(new AjaxSubmitLink("send") {
            @Override
            protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
                if (!form.hasError()) {
                    Message message = DashboardPage.this.form.getModelObject();

                    messageDAO.postMessage(message);
                    DashboardPage.this.form.setModelObject(new Message());

                    target.add(form);
                    target.add(feedbackPanel);
                    target.add(messagesPaneContainer);
                }
            }

            @Override
            protected void onError(AjaxRequestTarget target, Form<?> form) {
                target.add(feedbackPanel);
            }
        });

        ListView<Message> messageListView = new ListView<Message>(
                "messagesPane",
                new AbstractReadOnlyModel<List<Message>>() {
                    @Override
                    public List<Message> getObject() {
                        return messageDAO.getMessages();
                    }
                }
        ) {
            @Override
            protected void populateItem(ListItem<Message> listItem) {
                listItem.add(new MessagePanel("message", listItem.getModel()));
            }
        };

        add(messagesPaneContainer = new WebMarkupContainer("messagesPaneContainer"));
        messagesPaneContainer.setOutputMarkupId(true);
        messagesPaneContainer.add(messageListView);
    }

}
