package kamil.pg.test.web.behavior;

import org.apache.wicket.Component;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.form.AjaxFormComponentUpdatingBehavior;
import org.apache.wicket.markup.html.form.FormComponent;

/**
 * @author Nechaev Alexander
 */
public class ModelUpdateBehavior extends AjaxFormComponentUpdatingBehavior {

    public ModelUpdateBehavior() {
        super("change");
    }

    @Override
    protected final void onUpdate(AjaxRequestTarget ajaxRequestTarget) {
        Component component = getComponent();
        if (component instanceof FormComponent) {
            component.setDefaultModelObject(((FormComponent) component).getConvertedInput());
        }
        onModelChange(ajaxRequestTarget);
    }

    @Override
    protected final void onError(AjaxRequestTarget target, RuntimeException e) {
        onUpdate(target);
    }

    protected void onModelChange(AjaxRequestTarget target) {}

}
