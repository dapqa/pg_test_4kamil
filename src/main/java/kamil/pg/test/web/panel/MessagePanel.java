package kamil.pg.test.web.panel;

import kamil.pg.test.core.dto.Message;
import org.apache.wicket.behavior.AttributeAppender;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.util.convert.IConverter;
import org.apache.wicket.util.convert.converter.DateConverter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * @author Nechaev Alexander
 */
public class MessagePanel extends Panel {

    public MessagePanel(String id, IModel<Message> model) {
        super(id);
        setDefaultModel(model instanceof CompoundPropertyModel ? model : new CompoundPropertyModel<>(model.getObject()));
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();

        add(new Label("content"));
        add(new Label("authorName"));
        add(new Label("postDate") {
            @SuppressWarnings("unchecked")
            @Override
            public <Date> IConverter<Date> getConverter(Class<Date> type) {
                return (IConverter<Date>) new DateConverter() {
                    @Override
                    public DateFormat getDateFormat(Locale locale) {
                        return new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
                    }
                };
            }
        });

        Message message = getMessage();
        if (message.getMessageColor() != null && message.getMessageColor().getHexColor() != null) {
            add(new AttributeAppender("style", "background-color: " + message.getMessageColor().getHexColor() + ";"));
        }
    }

    private Message getMessage() {
        return (Message) getDefaultModelObject();
    }

}
