package kamil.pg.test.util;

import kamil.pg.test.core.dto.base.BaseDTO;

/**
 * @author Nechaev Alexander
 */
public class CommonUtils {

    public static <T> boolean equals(T a, T b) {
        return a == null ? b == null : a.equals(b);
    }

    public static <T extends BaseDTO> boolean equalsById(T a, T b) {
        return a == null ? b == null : equals(a.getId(), b.getId());
    }

    public static <T extends BaseDTO> Long getId(T obj) {
        return obj != null ? obj.getId() : null;
    }

    public static <T> T nvl(T obj, T ifNull) {
        return obj != null ? obj : ifNull;
    }

}
