package kamil.pg.test.core.dao;

import kamil.pg.test.core.dto.Message;
import kamil.pg.test.core.dto.MessageColor;

import java.util.List;

/**
 * @author Nechaev Alexander
 */
public interface MessageDAO {

    List<Message> getMessages();

    long postMessage(Message message);

    List<MessageColor> getMessageColors();

}
