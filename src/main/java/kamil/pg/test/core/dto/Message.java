package kamil.pg.test.core.dto;

import kamil.pg.test.core.dto.base.BaseDTO;
import kamil.pg.test.util.CommonUtils;

import java.util.Date;

/**
 * @author Nechaev Alexander
 */
public class Message extends BaseDTO {

    private String authorName;
    private String content;
    private Date postDate;
    private MessageColor messageColor;

    @SuppressWarnings("RedundantIfStatement")
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Message that = (Message) o;

        if (!CommonUtils.equals(authorName, that.authorName)) return false;
        if (!CommonUtils.equals(content, that.content)) return false;
        if (!CommonUtils.equals(postDate, that.postDate)) return false;
        if (!CommonUtils.equalsById(messageColor, that.messageColor)) return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (authorName != null ? authorName.hashCode() : 0);
        result = 31 * result + (content != null ? content.hashCode() : 0);
        result = 31 * result + (postDate != null ? postDate.hashCode() : 0);
        return result;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getPostDate() {
        return postDate;
    }

    public void setPostDate(Date postDate) {
        this.postDate = postDate;
    }

    public MessageColor getMessageColor() {
        return messageColor;
    }

    public void setMessageColor(MessageColor messageColor) {
        this.messageColor = messageColor;
    }
}
