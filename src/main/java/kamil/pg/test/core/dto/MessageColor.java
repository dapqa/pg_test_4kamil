package kamil.pg.test.core.dto;

import kamil.pg.test.core.dto.base.BaseDTO;
import kamil.pg.test.util.CommonUtils;

/**
 * @author Nechaev Alexander
 */
public class MessageColor extends BaseDTO {

    private String hexColor;
    private String name;

    @SuppressWarnings("RedundantIfStatement")
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        MessageColor that = (MessageColor) o;

        if (!CommonUtils.equals(hexColor, that.hexColor)) return false;
        if (!CommonUtils.equals(name, that.name)) return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (hexColor != null ? hexColor.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    public String getHexColor() {
        return hexColor;
    }

    public void setHexColor(String hexColor) {
        this.hexColor = hexColor;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
