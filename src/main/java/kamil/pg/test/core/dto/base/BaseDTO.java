package kamil.pg.test.core.dto.base;

import kamil.pg.test.util.CommonUtils;

import java.io.Serializable;

/**
 * @author Nechaev Alexander
 */
public class BaseDTO implements Serializable {

    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BaseDTO that = (BaseDTO) o;

        return CommonUtils.equals(id, that.id);

    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}