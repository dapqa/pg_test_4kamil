package kamil.pg.test.jdbc.base;

import org.springframework.jdbc.core.ArgumentPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import javax.sql.DataSource;
import java.io.Serializable;
import java.sql.PreparedStatement;

/**
 * @author Nechaev Alexander
 */
public class PGTestJdbcTemplate extends JdbcTemplate implements Serializable {

    public PGTestJdbcTemplate(DataSource dataSource) {
        super(dataSource);
    }

    public Long insertReturnId(String sql, Object... args) {
        PreparedStatementSetter preparedStatementSetter = new ArgumentPreparedStatementSetter(args);
        KeyHolder keyHolder = new GeneratedKeyHolder();

        update(connection -> {
            PreparedStatement ps = connection.prepareStatement(sql, new String[]{ "id" });
            preparedStatementSetter.setValues(ps);
            return ps;
        }, keyHolder);

        return keyHolder.getKey().longValue();
    }

}
