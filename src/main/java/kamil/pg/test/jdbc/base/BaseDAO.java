package kamil.pg.test.jdbc.base;

import kamil.pg.test.core.dto.base.BaseDTO;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.io.Serializable;

/**
 * @author Nechaev Alexander
 */
public class BaseDAO implements Serializable {

    protected PGTestJdbcTemplate jdbcTemplate;

    @Resource
    public final void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new PGTestJdbcTemplate(dataSource);
    }

    protected <T extends BaseDTO> boolean isNew(T dto) {
        return dto.getId() == null;
    }

}