package kamil.pg.test.jdbc.impl;

import kamil.pg.test.core.dao.MessageDAO;
import kamil.pg.test.core.dto.Message;
import kamil.pg.test.core.dto.MessageColor;
import kamil.pg.test.jdbc.base.BaseDAO;
import kamil.pg.test.util.CommonUtils;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * @author Nechaev Alexander
 */
@Service("messageDAO")
public class MessageDAOImpl extends BaseDAO implements MessageDAO {

    private static final RowMapper<Message> messageRowMapper = (rs, i) -> {
        Message message = new Message();

        message.setId(rs.getLong("id"));
        message.setAuthorName(rs.getString("author_name"));
        message.setContent(rs.getString("content"));
        message.setPostDate(rs.getTimestamp("post_date"));

        MessageColor messageColor = new MessageColor();
        messageColor.setId(rs.getLong("message_color_id"));
        messageColor.setHexColor(rs.getString("message_color_hex_color"));
        messageColor.setName(rs.getString("message_color_name"));
        message.setMessageColor(messageColor);

        return message;
    };

    private static final RowMapper<MessageColor> messageColorRowMapper = (rs, i) -> {
        MessageColor messageColor = new MessageColor();

        messageColor.setId(rs.getLong("id"));
        messageColor.setHexColor(rs.getString("hex_color"));
        messageColor.setName(rs.getString("name"));

        return messageColor;
    };

    @Override
    @Transactional(readOnly = true)
    public List<Message> getMessages() {
        return jdbcTemplate.query("select * from message_v order by post_date desc", messageRowMapper);
    }

    @Override
    @Transactional
    public long postMessage(Message message) {
        if (!isNew(message)) {
            // post значит, что для новых строк - insert, для обновленных - update
            throw new UnsupportedOperationException("Обновление данных не реализовано");
        }

        return jdbcTemplate.insertReturnId(
                "insert into messages(author_name, content, post_date, message_color_id) values(?, ?, ?, ?)",
                message.getAuthorName(),
                message.getContent(),
                CommonUtils.nvl(message.getPostDate(), new Date()),
                CommonUtils.getId(message.getMessageColor())
        );
    }

    @Override
    @Transactional(readOnly = true)
    public List<MessageColor> getMessageColors() {
        return jdbcTemplate.query("select * from message_color_v", messageColorRowMapper);
    }
}
