create table message_color (
    id			bigserial primary key,
    hex_color   varchar(7) not null,
    name        varchar(30) not null
);

-- Называется messages, а не message, потому что message - зарезервированное слово
create table messages (
    id			        bigserial primary key,
    author_name         varchar(100)  not null,
    content		        varchar(1000) not null,
    post_date	        timestamp not null default statement_timestamp(),
    message_color_id    bigint
);

alter table messages add constraint message_message_color_fk foreign key (message_color_id) references message_color(id);

------------------------------

insert into message_color(hex_color, name) values ('#dff0d8', 'Зелёный');
insert into message_color(hex_color, name) values ('#d9edf7', 'Синий');
insert into message_color(hex_color, name) values ('#fcf8e3', 'Жёлтый');
insert into message_color(hex_color, name) values ('#f2dede', 'Красный');

