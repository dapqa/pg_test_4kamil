create or replace view message_color_v as select * from message_color;

create or replace view message_v as
select
    m.id,
    m.author_name,
    m.content,
    m.post_date,
    m.message_color_id,
    mc.hex_color as message_color_hex_color,
    mc.name as message_color_name
from messages m
left outer join message_color as mc on m.message_color_id = mc.id;